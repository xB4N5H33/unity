﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

	public int MyCurrentHealth = 100;
	GameManager gm;
	public GameObject BulletPoint;
	public GameObject bulletPrefab;
	Rigidbody rb;


	// Use this for initialization
	void Start ()
	{
		// Find object, then find script on it
		//GameObject theGM =  GameObject.Find ("GameManager");
		GameObject theGM = GameObject.FindGameObjectWithTag ("GameManager");
		gm = theGM.GetComponent<GameManager> ();
		rb = GetComponent<Rigidbody> ();
		MyCurrentHealth = gm.GetPlayerStartingHealth ();
	}

	//MyCurrentHealth =  gm.PlayerHealth;
		
			

	
	// Update is called once per frame
	void Update ()
	{
		
		// Do damage to   player (walking on fire)
		//MyCurrentHealth -=   10;
		if (Input.GetButtonDown ("Fire1")) {
			GameObject newBullet = GameObject.Instantiate (bulletPrefab);
			newBullet.transform.position = BulletPoint.transform.position;
			rb.AddForce (gameObject.transform.forward * 5, ForceMode.Impulse);
			 
	


		}
	}
}